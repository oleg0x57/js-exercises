var nearestCells = [
    [0, -1], 
    [1, 0], 
    [0, 1], 
    [-1, 0]
];
function initGameField(rootDom, initialValues){
    var table = document.createElement('table');
    for(var i = 0; i < 4; i++){
        var tr = document.createElement('tr')
        for(var j = 0; j < 4; j++){
            var cellValue = initialValues[4 * i + j];
            td = document.createElement('td');
            td.innerHTML = cellValue;
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    rootDom.appendChild(table);
}
function shuffle(array){
    var j, x, i;
    for (i = array.length; i; i -= 1) {
        j = Math.floor(Math.random() * i);
        x = array[i - 1];
        array[i - 1] = array[j];
        array[j] = x;
    }
}
function startListen(rootDom){
    rootDom.onclick = function(event){
        var target = event.target;
        if(target.tagName != 'TD'){
            return;
        }
        var currentState = doTurn(target.cellIndex, target.parentElement.rowIndex, target.parentElement.parentElement);
        if(isFinished(currentState)){
            alert('Победа!');
        }
    }
}
function checkTurn(x, y, table){
    for(var i in nearestCells){
        var x1 = x + nearestCells[i][0];
        var y1 = y + nearestCells[i][1];
        if(table.rows[y1] !== undefined && table.rows[y1].cells[x1] !== undefined && table.rows[y1].cells[x1].innerHTML === '16'){
            return [x1, y1];
        }
    }
    return null;
}
function doTurn(x, y, table){
    var currentState = [];
    var direction = checkTurn(x, y, table);
    if(direction !== null){
        table.rows[direction[1]].cells[direction[0]].innerHTML = table.rows[y].cells[x].innerHTML;
        table.rows[y].cells[x].innerHTML = 16;
    }
    for(var i = 0; i < 4; i++){
        for(var j = 0; j < 4; j++){
            currentState.push(table.rows[i].cells[j].innerHTML);
        }
    }
    return currentState;
}
function isFinished(currentState){
    var finishState = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
    return finishState === currentState;
}
function startGame(rootDom, initialValues){
    shuffle(initialValues)
    initGameField(rootDom, initialValues);
    startListen(rootDom);
}